# This is a main Python script.
from qrng import qrng
from simulator import SingleQubitSimulator

if __name__ == '__main__':
    qsim = SingleQubitSimulator()
    for idx_sample in range(10):
        random_sample = qrng(qsim)
        print(f"Our QRNG returned {random_sample}.")
