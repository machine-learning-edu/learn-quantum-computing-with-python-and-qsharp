#!/bin/env python
# -*- coding: utf-8 -*-
##
# qrng.py: Defines and runs a quantum random number generator (qrng) using the interface defined
#   in interface.py and the simulator in simulator.py
##

from interface import QuantumDevice


def qrng(device: QuantumDevice) -> bool:
    with device.using_qubit() as q:
        q.h()
        return q.measure()
